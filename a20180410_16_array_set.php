<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php
$ar2 = [
    'name' => 'bill',
    'age' => 27,
];


$ar3 = $ar2;
$ar4 = &$ar2;

$ar2['age'] = 30;

print_r($ar2);
print_r($ar3);
print_r($ar4);


?>
</pre>
</body>
</html>