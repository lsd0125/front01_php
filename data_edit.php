<?php
require __DIR__ . '/__db_connect.php';

$page_name = 'data_edit';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
if(empty($sid)){
    header('Location: data_list.php');
    exit;
}

$rs = $mysqli->query("SELECT * FROM address_book WHERE sid=$sid");
if($rs->num_rows < 1){
    header('Location: data_list.php');
    exit;
}

$row = $rs->fetch_assoc();


if(isset($_POST['name'])) {

    // 檢查各欄位值是否符合要求

    $sql = "UPDATE `address_book` SET `name`=?,`mobile`=?,`email`=?,`birthday`=?,`address`=? WHERE sid=?";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssssi',
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['birthday'],
        $_POST['address'],
        $sid
        );

    $stmt->execute();
    $affected_rows =  $stmt->affected_rows;

    $stmt->close();


}
$name = isset($_POST['name']) ? $_POST['name'] : $row['name'];
$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : $row['mobile'];
$email = isset($_POST['email']) ? $_POST['email'] : $row['email'];
$birthday = isset($_POST['birthday']) ? $_POST['birthday'] : $row['birthday'];
$address = isset($_POST['address']) ? $_POST['address'] : $row['address'];


?>
<?php include __DIR__. '/__html_head.php' ?>
<div class="container">

    <?php include __DIR__. '/__navbar.php' ?>

    <?php if(isset($affected_rows)): ?>
        <?php if($affected_rows==1): ?>
            <div class="alert alert-success" role="alert">
                資料修改完成
            </div>
        <?php elseif($affected_rows==-1): ?>
            <div class="alert alert-danger" role="alert">
                手機號碼重複了
            </div>
        <?php else: ?>
            <div class="alert alert-warning" role="alert">
                資料沒有變更
            </div>
        <?php endif ?>
    <?php endif ?>

    <div class="row" style="margin-top: 20px">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    編輯資料
                </div>
                <div class="card-body">

                    <form method="post" onsubmit="return formCheck()">
                        <div class="form-group">
                            <label for="name">** 姓名</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="請輸入姓名"
                            value="<?= $name ?>">
                            <small id="name_info" class="form-text text-muted">請輸入正確的姓名</small>
                        </div>

                        <div class="form-group">
                            <label for="mobile">** 手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder=""
                                   value="<?= $mobile ?>">
                            <small id="mobile_info" class="form-text text-muted">請輸入十位數的手機號碼</small>
                        </div>

                        <div class="form-group">
                            <label for="email">** 電子郵件</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder=""
                                   value="<?= $email ?>">
                            <small id="email_info" class="form-text text-muted">請輸入正確的email</small>
                        </div>

                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder=""
                                   value="<?= $birthday ?>">
                        </div>

                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea name="address" id="address"  class="form-control" cols="30" rows="3"><?= $address ?></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">修改</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<style>
    small.form-text.text-muted {
        color: red !important;
        display: none;
    }
</style>
<script>
    var f_name = $('#name');
    var f_mobile = $('#mobile');
    var f_email = $('#email');

    function formCheck(){

        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var isPass = true;

        $('#name_info').hide();
        $('#mobile_info').hide();
        $('#email_info').hide();

        if(f_name.val().length < 2){
            isPass = false;
            $('#name_info').show();
        }

        if(f_mobile.val().length < 10){
            isPass = false;
            $('#mobile_info').show();
        }

        if(! pattern.test(f_email.val())){
            isPass = false;
            $('#email_info').show();
        }


        return isPass;
    }

</script>

<?php include __DIR__. '/__html_foot.php' ?>
