<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<form id="form1" action="" method="post" onsubmit="return false">

    <label for="a">a:</label>
    <input type="text" name="a" id="a"><br>

    <label for="b">b:</label>
    <input type="text" name="b" id="b"><br>

    <input type="button" value="送出" onclick="checkData()">
    
</form>
<div id="myd"></div>

<script>
    function checkData(){
        console.log(  $('#form1').serialize());

        $.post('a20180410_13_plus_api.php', $('#form1').serialize(), function(data){
            $('#myd').text(data);
        });

    }

</script>
</body>
</html>