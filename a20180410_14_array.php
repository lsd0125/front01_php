<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php
$ar0 = array(2,4,6,8,10);

$ar1 = [2,4,6,8,10];

$ar1[] = 13; // array_push()


print_r($ar1);
var_dump($ar1);


$ar2 = array(
    'name' => 'bill',
    'age' => 27,
    'gender' => 'male',
);

print_r($ar2);


?>
</pre>
</body>
</html>