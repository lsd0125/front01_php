-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2018-04-17 05:02:57
-- 伺服器版本: 10.1.31-MariaDB
-- PHP 版本： 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `mytest`
--

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `mobile`, `email`, `birthday`, `address`, `created_at`) VALUES
(22, '李小明1', '0.02312959360250234', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(23, '李小明2', '0.4700536741596215', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(24, '李小明3', '0.28087962072424555', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(25, '李小明4', '0.9942371118760082', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(26, '李小明5---', '0.12854672794094935', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(27, '李小明6', '0.6600223348103672', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(28, '李小明7', '0.9144715209626327', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(29, '李小明8', '0.5922906311157072', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(30, '李小明9', '0.21803862342428287', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(31, '李小明10', '0.3133212545079377', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(32, '李小明1a', '0.9124904330004849', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(33, '李小明2a', '0.6224884322122562', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(34, '李小明3a', '0.37497089279347184', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(35, '李小明4a', '0.0073891980642352235', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(36, '李小明5a', '0.9120333426744056', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(37, '李小明6a', '0.5379991499129675', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(38, '李小明7a', '0.9538957522752656', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(39, '李小明8a', '0.15548134237107014', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(40, '李小明9a', '0.9157194857631992', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(41, '李小明10a', '0.11215343243643029', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(42, '李小明10a5675', '0.8136087356261991', 'test@gmail.com', '2000-04-06', '台北市', NULL),
(43, 'sdfgdf', '0.7315834115553492', 'sdgfdf@sdg.com', '0000-00-00', '', NULL),
(44, '陳小華\'s', '0.21709088163179427', 'sdgsd@sdfsf.com', '0000-00-00', 'serdfgdg', NULL),
(45, 'sdg', '0.8907042042265685', 'sdgfdf@sdg.com', '1990-12-12', 'sdfgd', '2018-04-16 11:54:12'),
(46, 'sdgfsd', '0.8022484069711048', 'sdgsd@sdfsf.com', '1990-12-20', '', '2018-04-17 09:45:15'),
(47, '吳天才', '0.3391294529094635', 'sdgfdf@sdg.com', '0000-00-00', '', '2018-04-17 09:58:49'),
(48, '吳天才test', '0.2889020743676481', 'sdgfdf@sdg.com', '0000-00-00', '', '2018-04-17 09:58:49'),
(49, 'dgfdfgd', '0.4271220438434948', 'sdgfdf@sdg.com', '2000-10-10', '<script>alert(\'爛芭樂\')</script>', '2018-04-17 10:11:46'),
(50, 'dgfdfgdtest', '0.26890402684817466', 'sdgfdf@sdg.com', '2000-10-10', '<script>alert(\'爛芭樂\')</script>', '2018-04-17 10:11:46'),
(52, 'sdgres', '1234567898', 'sdgfdf@sdg.com', '0000-00-00', '', '2018-04-17 10:53:35'),
(53, '213123', '0911123111', 'sfs@gmail.com', '0000-00-00', '', '2018-04-17 10:56:17');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
