<?php
require __DIR__ . '/__db_connect.php';

$result = [];

// 設定一頁有幾筆
$per_page = 5;

$result['per_page'] = $per_page;

// 總共有幾筆
    // SELECT COUNT(*) FROM address_book
    // SELECT COUNT(`sid`) FROM address_book
$t_sql = "SELECT COUNT(1) FROM address_book";
$rs = $mysqli->query($t_sql);
$total = $rs->fetch_row()[0];
$result['items_num'] = $total;

$total_pages = ceil($total/$per_page);
$result['pages_num'] = $total_pages;

// 用戶要看第幾頁
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$page = $page<1 ? 1 : $page;
$page = $page>$total_pages ? $total_pages : $page;
$result['page'] = $page;

$sql = sprintf("SELECT * FROM address_book ORDER BY sid DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
$rs = $mysqli->query($sql);

$result['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$result['success'] = true;

//print_r($result);

echo json_encode($result, JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES);


