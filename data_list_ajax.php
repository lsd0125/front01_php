<?php
require __DIR__ . '/__db_connect.php';

$page_name = 'data_list_ajax';

?>
<?php include __DIR__. '/__html_head.php' ?>
<div class="container">

    <?php include __DIR__. '/__navbar.php' ?>

    <div class="" style="margin-top: 30px">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>sid</td>
                <td>姓名</td>
                <td>手機</td>
                <td>電郵</td>
                <td>生日</td>
                <td>地址</td>
                <td>移除</td>
                <td>編輯</td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div>

        <nav aria-label="Page navigation example">
            <ul class="pagination">
            </ul>
        </nav>

    </div>

</div>

<script type="text/x-template" id="tr_tpl">
    <tr>
        <td><%= sid %></td>
        <td><%= name %></td>
        <td><%= mobile %></td>
        <td><%= email %></td>
        <td><%= birthday %></td>
        <td><%= address %></td>
        <td><a href="javascript: delete_it(<%= sid %>)">
                <i class="fas fa-trash-alt"></i>
            </a>
        </td>
        <td><a href="data_edit.php?sid=<%= sid %>">
                <i class="fas fa-pencil-alt"></i>
            </a>
        </td>
    </tr>
</script>



<script>
    var tr_tpl = $('#tr_tpl').text();
    var tr_tpl_f = _.template(tr_tpl);
    var tbody = $('tbody');
    let tr_tpl2 = `<tr>
        <td><%= sid %></td>
        <td><%= name %></td>
        <td><%= mobile %></td>
        <td><%= email %></td>
        <td><%= birthday %></td>
        <td><%= address %></td>
        <td><a href="javascript: delete_it(<%= sid %>)">
                <i class="fas fa-trash-alt"></i>
            </a>
        </td>
        <td><a href="data_edit.php?sid=<%= sid %>">
                <i class="fas fa-pencil-alt"></i>
            </a>
        </td>
    </tr>`;
    let tr_tpl_f2 = _.template(tr_tpl2);

    let pag_1 = `<li class="page-item <%= isDisabled %>">
                    <a class="page-link" href="#1">
                        <i class="fas fa-angle-double-left"></i>
                    </a>
                </li>`;
    let pag_2 = `<li class="page-item <%= isDisabled %>">
                    <a class="page-link" href="#<%= i %>">
                        <%= i %>
                    </a>
                </li>`;
    let pag_3 = `<li class="page-item <%= isDisabled %>">
                    <a class="page-link" href="#<%= total_pages %>">
                        <i class="fas fa-angle-double-right"></i>
                    </a>
                </li>`;
    let pagination = $('.pagination');
    let pag_1f = _.template(pag_1);
    let pag_2f = _.template(pag_2);
    let pag_3f = _.template(pag_3);


    let afterLoaded = function(data){
        let i, row, s;
        console.log(data);
        tbody.empty();

        for(i=0; i<data.data.length; i++){
            row = data.data[i];

            //tbody.append(tr_tpl_f(row));
            for(s in row){
                if(row[s]) {
                    row[s] = row[s].split('<').join('&lt;');
                    row[s] = row[s].split('>').join('&gt;');
                }
            }
            tbody.append(tr_tpl_f2(row));

        }
        pagination.empty();
        if(data.page===1){
            pagination.append( pag_1f({isDisabled:'disabled'}) );
        } else {
            pagination.append( pag_1f({isDisabled:''}) );
        }

        for(i=1; i<=data.pages_num; i++){
            if(i===data.page){
                pagination.append( pag_2f({isDisabled:'disabled', i:i}) );
            } else {
                pagination.append( pag_2f({isDisabled:'', i:i}) );
            }
        }

        if(data.page===data.pages_num){
            pagination.append( pag_3f({isDisabled:'disabled', total_pages:data.pages_num}) );
        } else {
            pagination.append( pag_3f({isDisabled:'', total_pages:data.pages_num}) );
        }

    };

    let whenHashChanged = function(){
        let p = parseInt(location.hash.slice(1));
        p = (p < 1) ? 1 : p;
        $.get('data_list_json.php', {page:p}, afterLoaded, 'json');
    };

    window.addEventListener('hashchange', whenHashChanged);
    whenHashChanged();



    function delete_it(sid){
        if(confirm('你確定要刪除編號為 '+sid+' 的資料嗎?')){
            location.href = 'data_delete.php?sid=' +sid;
        }
    }

</script>
<?php include __DIR__. '/__html_foot.php' ?>
