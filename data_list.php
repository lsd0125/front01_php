<?php
require __DIR__ . '/__db_connect.php';

$page_name = 'data_list';

// 設定一頁有幾筆
$per_page = 5;

// 總共有幾筆
    // SELECT COUNT(*) FROM address_book
    // SELECT COUNT(`sid`) FROM address_book
$t_sql = "SELECT COUNT(1) FROM address_book";
$rs = $mysqli->query($t_sql);
$total = $rs->fetch_row()[0];
$total_pages = ceil($total/$per_page);

// 用戶要看第幾頁
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$page = $page<1 ? 1 : $page;
$page = $page>$total_pages ? $total_pages : $page;


$sql = sprintf("SELECT * FROM address_book ORDER BY sid DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);

$result = $mysqli->query($sql);



?>
<?php include __DIR__. '/__html_head.php' ?>
<div class="container">

    <?php include __DIR__. '/__navbar.php' ?>
    <div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=1">
                        <i class="fas fa-angle-double-left"></i>
                    </a>
                </li>
                <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $page-1 ?>">
                        <i class="fas fa-angle-left"></i>
                    </a></li>
                <li class="page-item"><a class="page-link">
                    <?= "$page / $total_pages" ?>
                    </a>
                </li>
                <li class="page-item <?= $page==$total_pages ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $page+1 ?>">
                        <i class="fas fa-angle-right"></i>
                    </a></li>
                <li class="page-item <?= $page==$total_pages ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $total_pages ?>">
                        <i class="fas fa-angle-double-right"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="" style="margin-top: 30px">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>sid</td>
                <td>姓名</td>
                <td>手機</td>
                <td>電郵</td>
                <td>生日</td>
                <td>地址</td>
                <td>移除</td>
                <td>編輯</td>
            </tr>
            </thead>
            <tbody>
            <?php while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?= htmlentities($row['sid']) ?></td>
                    <td><?= htmlentities($row['name']) ?></td>
                    <td><?= htmlentities($row['mobile']) ?></td>
                    <td><?= htmlentities($row['email']) ?></td>
                    <td><?= htmlentities($row['birthday']) ?></td>
                    <td><?= htmlentities($row['address']) ?></td>
                    <td><a href="javascript: delete_it(<?= $row['sid'] ?>)">
                        <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                    <td><a href="data_edit.php?sid=<?= $row['sid'] ?>">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                    </td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>
    </div>
    <div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=1">
                        <i class="fas fa-angle-double-left"></i>
                    </a>
                </li>
                <?php for($i=1; $i<=$total_pages; $i++): ?>
                <li class="page-item <?= $page==$i ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $i ?>">
                        <?= $i ?>
                    </a>
                </li>
                <?php endfor ?>
                <li class="page-item <?= $page==$total_pages ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $total_pages ?>">
                        <i class="fas fa-angle-double-right"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

</div>
<script>
    function delete_it(sid){
        if(confirm('你確定要刪除編號為 '+sid+' 的資料嗎?')){
            location.href = 'data_delete.php?sid=' +sid;
        }
    }

</script>
<?php include __DIR__. '/__html_foot.php' ?>
